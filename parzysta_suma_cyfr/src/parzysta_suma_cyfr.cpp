//============================================================================
// Name        : parzysta_suma_cyfr.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout
			<< "Program pobiera okreslona ilosc liczb do tablicy i zwraca te, ktorych suma jest parzysta. Wprowadzenie cyfry 0 przerywa wpisywanie liczb."
			<< endl;
	int const maxSize(100);
	int t[maxSize]; // array declaration
	int i(0);
	for (i = 0; i < maxSize; ++i) { // enters array elements
		cout << "Wprowadz " << i << " element talicy: ";
		cin >> t[i];
		if (t[i] == 0)
			break;
	}
	for (int j = 0; j < i; ++j) {
		int sum(0);
		int temp(0);
		temp = t[j];
		while (temp != 0) {
			sum += temp % 10;
			temp /= 10;
		}
		if ((sum & 1) == 0)
			cout << t[j] << " "; // writes out array elements which sum of digits is even
	}
	return 0;
}
