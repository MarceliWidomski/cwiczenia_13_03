//============================================================================
// Name        : unikalne_wartosci_tablicy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program zlicza unikalne wartosci w tablicy" << endl;
	int maxSize(1000);
	int t[maxSize];
	int size(1001);
	while (size > maxSize) {
		cout << "Podaj ilosc elementow tablicy " << endl;
		cin >> size;
		if (size > maxSize)
			cout
					<< "Podany rozmiar tablicy jest zbyt duzy. Maksymalny rozmiar tablicy wynosi 1000. "
					<< endl;
	}
	int temp;
	cout << "Wpisz kolejne elementy tablicy: " << endl;
	for (int i = 0; i < size; ++i) {
		cin >> temp;
		t[i] = temp;
	}
	for (int j = 0; j < size; ++j) {
		int temp = t[j];
		for (int k = j + 1; k < size; ++k) {
			if ((temp == t[k])) {
				t[j] = 0;
				t[k] = 0;
			}
		}
	}
	int unique(0);
	for (int i = 0; i < size; ++i) {
		if (t[i] != 0)
			++unique;
	}
	cout << "Liczba unikalnych wartosci wynosi " << unique << endl;
	return 0;
}
