#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje sume elementow tablicy" << endl;
	int maxSize(1000);
	int t[maxSize];
	int size(1001);
	while (size > maxSize) {
		cout << "Podaj ilosc elementow tablicy " << endl;
		cin >> size;
		if (size > maxSize)
			cout
					<< "Podany rozmiar tablicy jest zbyt duzy. Maksymalny rozmiar tablicy wynosi 1000. "
					<< endl;
	}
	cout << "Wpisz kolejne elementy tablicy: " << endl;
	for (int i = 0; i < size; ++i) {
		cin >> t[i];
	}
	int sum(0);
	for (int i = 0; i < size; ++i) {
		sum+=t[i];
		}
		cout << "Suma elementow tablicy wynosi: " << sum << endl;
	return 0;
}
