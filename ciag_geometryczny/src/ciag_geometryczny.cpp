//============================================================================
// Name        : ciag_geometryczny.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int const maxSize(1000);
	int t[maxSize]; // array declaration
	cout << "Program wypelnia tablice ciagiem geometrycznym."
				<< endl;
	cout << "Podaj pierwszy wyraz ciagu: ";
	int first;
	cin >> first;
	cout << "Podaj iloraz ciagu: ";
	int quotient;
	cin >> quotient;
	cout << "Podaj ilosc wyrazow ciagu: ";
	int number;
	cin >> number;
	t[0]=first;
	for (int i = 1; i <=number;++i){
		t[i]=t[i-1]*quotient;
		cout << t[i-1] << " ";
	}

	return 0;
}
