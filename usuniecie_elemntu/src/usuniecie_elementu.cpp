#include <iostream>
using namespace std;

int main() {
	cout << "Program usuwa element z tablicy z podanej pozycji" << endl;
	int maxSize(1000);
	int t[maxSize] = { };
	int size(1001);
	int elementNumber(0);
	while (size > maxSize) {
		cout << "Podaj rozmiar tablicy: ";
		cin >> size;
		if (size > maxSize)
			cout << "Przekroczony maksymalny rozmiar tablicy. ";
	}
	for (int i = 0; i < size; ++i) {
		t[i] = i;
		//cout << t[i] << " ";
	}
	int option(1);
	while (option == 1) {
		cout << "Podaj numer elementu ktory chcialbys usunac: ";
		cin >> elementNumber;
		if (elementNumber > size - 1)
			cout << "Element o podanym numerze nie istnieje " << endl;
		else{
		cout << "Usunieto element." << endl;
		for (int i = elementNumber; i<size-1; ++i){
			t[i]=t[i+1];
		}
		t[size-1]=0;
		size-=1;
		}
		option=0;
		while (option != 1 && option != 2) {
			cout << "Czy chcesz usunac kolejny element? " << endl;
			cout << "1) Tak" << endl;
			cout << "2) Nie" << endl;
			cin >> option;
			if (option != 1 && option != 2)
				cout << "Niewlasciwa opcja. Sprobuj ponownie. " << endl;
		}
	}
	cout << "Aktualna tablica to: " << endl;
	for (int i =0; i<size; ++i)
		cout << t[i] << " ";
	return 0;
}
