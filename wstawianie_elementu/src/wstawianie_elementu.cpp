#include <iostream>
using namespace std;

int main() {
	cout << "Program wstawia element do tablicy na podana pozycje" << endl;
	int maxSize(1000);
	int t[maxSize] = { };
	int elementNumber(1001);
	int maxElNumber(0);
	int option(1);
	while (option == 1) {
		elementNumber=1001;
		while (elementNumber > (maxSize - 1)) {
			cout << "Podaj numer w tablicy na ktory chcesz wstawic element: "
					<< endl;
			cin >> elementNumber;
			if (elementNumber > maxElNumber)
				maxElNumber = elementNumber;
			if (elementNumber > maxSize)
				cout
						<< "Element tablicy o podanym numerze nie istnieje. Maksymalny rozmiar tablicy wynosi 1000. "
						<< endl;
		}
			cout << "Podaj wartosc elementu: ";
			int elementValue;
			cin >> elementValue;
			t[elementNumber] = elementValue;
			option = 0;
			while (option != 1 && option != 2) {
			cout << "Czy chcesz podac kolejna wartosc? " << endl;
			cout << "1) Tak" << endl;
			cout << "2) Nie" << endl;
			cin >> option;
			if (option != 1 && option != 2)
				cout << "Niewlasciwa opcja. Sprobuj ponownie. " << endl;
		}
	}
	cout << "Utworzona tablica to: " << endl;
	for (int i = 0; i <= maxElNumber; ++i)
		cout << t[i] << endl;
	return 0;
}
