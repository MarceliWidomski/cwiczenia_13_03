#include <iostream>
using namespace std;

int main() {
	cout << "Program zlicza czestotliwosc wystepowania elementow w tablicy" << endl;
	int maxSize(1000);
	int t[maxSize];
	int size(1001);
	while (size > maxSize) {
		cout << "Podaj ilosc elementow tablicy " << endl;
		cin >> size;
		if (size > maxSize)
			cout
					<< "Podany rozmiar tablicy jest zbyt duzy. Maksymalny rozmiar tablicy wynosi 1000. "
					<< endl;
	}
	cout << "Wpisz kolejne elementy tablicy: " << endl;
	for (int i = 0; i < size; ++i) {
		cin >> t[i];
	}
	int charZero(0);
	for (int i = 0; i < size; ++i) {
		if (t[i] == 0)
			++charZero;
	}
	if (charZero > 0){
		cout << "Wartosc 0 wystepuje w tablicy " << charZero << " razy." << endl;
	}
	for (int i = 0; i < size; ++i) {
		int multiple(1);
		if (t[i] == 0)
			continue;
		else {
			for (int j = i + 1; j < size; ++j) {
				if (t[i] == t[j]) {
					t[j] = 0;
					++multiple;
				}
			}
		}
		cout << "Wartosc " << t[i] << " wystepuje w tablicy " << multiple
				<< " razy." << endl;
	}
	return 0;
}
