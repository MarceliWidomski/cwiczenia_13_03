#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje roznice dwoch kolejnych elementow tablicy" << endl;
	int maxSize(1000);
	int t[maxSize];
	int size(1001);
	while (size > maxSize) {
		cout << "Podaj ilosc elementow tablicy " << endl;
		cin >> size;
		if (size > maxSize)
			cout
					<< "Podany rozmiar tablicy jest zbyt duzy. Maksymalny rozmiar tablicy wynosi 1000. "
					<< endl;
	}
	cout << "Wpisz kolejne elementy tablicy: " << endl;
	for (int i = 0; i < size; ++i) {
		cin >> t[i];
	}
	int remainder(0);
	for (int i = 1; i < size; ++i) {
		remainder = t[i-1]-t[i];
		cout << "Roznica elementu " << i-1 << " i elementu " << i << " wynosi: " << remainder << endl;
		}
	return 0;
}
