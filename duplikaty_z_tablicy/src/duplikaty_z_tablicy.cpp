//============================================================================
// Name        : duplikaty_z_tablicy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program zlicza duplikaty w tablicy" << endl;
	int maxSize(1000);
	int t[maxSize];
	int size(1001);
	while (size > maxSize) {
		cout << "Podaj ilosc elementow tablicy " << endl;
		cin >> size;
		if (size > maxSize)
			cout
					<< "Podany rozmiar tablicy jest zbyt duzy. Maksymalny rozmiar tablicy wynosi 1000. "
					<< endl;
	}
	cout << "Wpisz kolejne elementy tablicy: " << endl;
	for (int i = 0; i < size; ++i) {
		cin >> t[i];
	}
	int charZero(0);
	for (int i = 0; i < size; ++i) {
		if (t[i] == 0)
			++charZero;
	}
	int character(0);
	if (charZero > 0){
		cout << "Wartosc 0 wystepuje w tablicy " << charZero << " razy." << endl;
		++character;
	}
	for (int i = 0; i < size; ++i) {
		int multiple(1);
		if (t[i] == 0)
			continue;
		else {
			for (int j = i + 1; j < size; ++j) {
				if (t[i] == t[j]) {
					t[j] = 0;
					++multiple;
				}
			}
		}
		++character;
		cout << "Wartosc " << t[i] << " wystepuje w tablicy " << multiple
				<< " razy." << endl;
	}
	cout << "W tablicy wystepuje " << character << " rodzajow znakow.";
	return 0;
}
