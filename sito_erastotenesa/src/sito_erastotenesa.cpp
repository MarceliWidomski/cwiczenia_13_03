//============================================================================
// Name        : sito_erastotenesa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje kolejne liczby pierwsze metoda sita Eratostenesa."
			<< endl;
	int const maxSize(1000);
	int actualSize(1001);
	int t[maxSize]; // array declaration
	while (actualSize > maxSize) {
		cout
				<< "Podaj koniec zakresu z ktorego beda pochodzic liczby pierwsze: ";

		cin >> actualSize;
		if (actualSize - 1 > maxSize)
			cout << "Zbyt duzy zakres. Maksywalny zakres wynosi " << maxSize
					<< endl;
	}
	for (int i = 0; i < actualSize - 1; ++i) // fills array with numbers from 2
		t[i] = i + 2;
	for (int j = 1; j < actualSize - 1; ++j) {
		if (t[j-1]!=0) { // prevents from dividing by 0 and loop will execute less times
			for (int k = j; k < actualSize - 1; ++k) {
				if (t[k] % t[j - 1] == 0) { // sets array elements as 0 if they aren't prime
					t[k] = 0;
				}
			}
		}
	}
	for (int i = 0; i < actualSize - 1; ++i) { // writes out prime numbers from array
		if (t[i] != 0)
			cout << t[i] << " ";
	}
	return 0;
}
