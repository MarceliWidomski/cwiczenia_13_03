//============================================================================
// Name        : wyszukanie_elementu.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
	cout << "Program zwraca indeks szukanego elementu " << endl;
	int maxSize(1000);
	int t[maxSize] = { };
	int size(1001);
	while (size > maxSize) {
		cout << "Podaj rozmiar tablicy: ";
		cin >> size;
		if (size > maxSize)
			cout << "Przekroczony maksymalny rozmiar tablicy. ";
	}
	for (int i = 0; i < size; ++i)
		t[i] = (rand()%size)+1;
	cout << "Podaj wartosc elementu ktory chcialbys znalezc: ";
	int elementValue;
	cin >> elementValue;
	bool exists(0);
	for (int i = 0; i < size; ++i) {
		if (t[i] == elementValue) {
			exists = 1;
			cout << "Wartosc znajduje sie pod indeksem " << i << endl;
		}
	}
	if (exists == 0)
		cout << "Szukana wartosc nie wystepuje w tablicy." << endl;
	cout << "Tablica: ";
	for (int i = 0; i < size; ++i)
			cout << t[i] << " ";
	return 0;
}
